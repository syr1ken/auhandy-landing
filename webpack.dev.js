const { merge } = require("webpack-merge");
const path = require("path");
const common = require("./webpack.common.js");

module.exports = merge(common, {
  mode: "development",
  // Server
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 1234,
    open: "chrome"
  },
  devtool: "eval-source-map",
  plugins: [require("autoprefixer")],
  module: {
    rules: [
      // CSS
      {
        test: /\.s[ac]ss$/i,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              sourceMap: false
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: false
            }
          }
        ]
      }
    ]
  }
});
