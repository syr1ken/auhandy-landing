// Import JQuery
import $ from "jquery";

// Slick Carousel
import "slick-carousel";

// Phone Mask
import IMask from "imask";

// Import Bootstrap
import "bootstrap";

// Font Awesome
import "@fortawesome/fontawesome-free/js/all.js";

// Import Custom Styles
import "../scss/styles.scss";

// Smooth Scroll
import * as SmoothScroll from "smooth-scroll";

// Validate
import Validate from "./validate.js";

// Images
const images = {
  favicon: require("../img/favicon-32x32.png"),
  logo: require("../img/logo.svg"),
  heroImg: require("../img/hero-img.png"),
  gear: require("../img/gear.png"),
  commnet: require("../img/comment.png"),
  gift: require("../img/gift.png"),
  like: require("../img/like.png"),
  gear: require("../img/gears.png"),
  lightBolt: require("../img/light-bolt.png"),
  pipe: require("../img/pipe.png"),
  wrench: require("../img/wrench.png"),
  testimonialsSaw: require("../img/testimonials-saw.png"),
  testimonialsNails: require("../img/testimonials-nails.png"),
  testimonialsPlane: require("../img/testimonials-plane.png"),
  testimonialsAxe: require("../img/testimonials-axe.png"),
  projectsBear: require("../img/projects-bear.png"),
  projectsLan: require("../img/projects-lan.png"),
  projectsReverse: require("../img/projects-lan-reverse.png"),
  projectsRabbit: require("../img/projects-rabbit.png"),
  servicesBg: require("../img/services-bg.png"),
  ctaBg: require("../img/cta-bg.png"),
  heroBg: require("../img/hero-bg.png"),
  rollerBrush: require("../img/roller-brush.png"),
  shop: require("../img/shop.png"),
  hammer: require("../img/hammer.png"),
  shovel: require("../img/shovel.png"),
  shovel: require("../img/features-img.png"),
  ctaFooter: require("../img/cta-footer-bg.png"),
  clock: require("../img/clock.png"),
  chevron: require("../img/chevron.png"),
  phone: require("../img/phone.png"),
  mark: require("../img/mark.png"),
  envelope: require("../img/envelope.png"),
  marker: require("../img/marker.png")
};

const elements = {
  searchform: $("#searchform"),
  searchBtn: $("#search__btn"),
  slickCarousel: $("#carousel"),
  carouselPrev: $("#carousel__prev"),
  carouselNext: $("#carousel__next"),
  modalOverlay: $("#modal__overlay"),
  closeModal: $("#close__modal"),
  learnMoreBtn: $("#btn__learnmore"),
  phoneInputs: $(".phone__input")
};

const elementStrings = {
  show: "show",
  active: "active",
  input: "input",
  form: "form",
  nameInput: "name__input",
  emailInput: "email__input",
  phoneInput: "phone__input",
  errorElement: "form__error",
  inputError: "error"
};

const slickSliderConfig = {
  dots: false,
  arrows: true,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  prevArrow: elements.carouselPrev,
  nextArrow: elements.carouselNext,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};

// Handlers
const toggleSearchForm = (e) => {
  $(elements.searchBtn).toggleClass(elementStrings.active);
  $(elements.searchform).toggleClass(elementStrings.show);
};

const closeModal = (e) => {
  elements.modalOverlay.removeClass(elementStrings.active);
};

const openModal = (e) => {
  elements.modalOverlay.addClass(elementStrings.active);
};

const error = (input, errorMessage) => {
  const error = input.parentNode.querySelector(`.${elementStrings.errorElement}`);
  error.classList.add(elementStrings.active);
  error.innerHTML = errorMessage;
  input.classList.add(elementStrings.inputError);
};

const success = (input) => {
  input.classList.remove(elementStrings.inputError);
};

const removeStates = (elements, states) => {
  elements.forEach((element) => {
    states.forEach((state) => {
      element.classList.remove(state);
    });
  });
};

const getFieldName = (element) => {
  return element.name.charAt(0).toUpperCase() + element.name.slice(1);
};

const validate = (e) => {
  e.preventDefault();

  let min, max;

  const errorElements = e.target.querySelectorAll(`.${elementStrings.errorElement}`);
  const nameInput = e.target.querySelector(`.${elementStrings.nameInput}`);
  const emailInput = e.target.querySelector(`.${elementStrings.emailInput}`);
  const phoneInput = e.target.querySelector(`.${elementStrings.phoneInput}`);

  removeStates([nameInput, emailInput, phoneInput], [elementStrings.inputError, elementStrings.inputSuccess]);
  removeStates(Array.prototype.slice.call(errorElements), [elementStrings.active]);

  Validate.checkRequired(nameInput, success, error, `${getFieldName(nameInput)} is required`);
  Validate.checkRequired(emailInput, success, error, `${getFieldName(emailInput)} is required`);
  Validate.checkRequired(phoneInput, success, error, `${getFieldName(phoneInput)} is required`);

  min = 3;
  max = 36;
  Validate.checkLength(nameInput, min, max, success, error, `Must be at least ${min} characters`, `Must be less than ${max} characters`);

  min = 7;
  max = 36;
  Validate.checkLength(emailInput, min, max, success, error, `Must be at least ${min} characters`, `Must be less than ${max} characters`);

  min = 16;
  max = 16;
  Validate.checkLength(phoneInput, min, max, success, error, `Must be at least ${min} characters`, `Must be less than ${max} characters`);

  Validate.checkEmail(emailInput, success, error, `Enter valid email`);
};

// Event Listeners
// Header Search Form
elements.searchBtn.on("click", toggleSearchForm);

// Caruosel
elements.slickCarousel.slick(slickSliderConfig);

// Modal
elements.closeModal.on("click", closeModal);
elements.learnMoreBtn.on("click", openModal);

// Phone Mask
Array.prototype.slice.call(elements.phoneInputs).forEach((input) => {
  IMask(input, {
    mask: "+{7}(000)000-00-00"
  });
});

window.addEventListener("click", (e) => {
  e.target === elements.modalOverlay[0] ? elements.modalOverlay.removeClass(elementStrings.active) : false;
});

// Smooth scroll
const scroll = new SmoothScroll('a[href*="#"]', {
  speed: 300
});

// Validation
$(elementStrings.form).on("submit", validate);
